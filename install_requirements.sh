#!/bin/bash

# 设置镜像源和信任的主机
mirror_url="http://pypi.douban.com/simple/"
trusted_host="pypi.douban.com"

# 逐行读取 requirements.txt 文件中的每个库名称，并尝试使用指定的镜像源进行安装
while read -r package; do
    if ! pip3 install "$package" -i "$mirror_url" --trusted-host "$trusted_host"; then
        echo "Failed to install $package"
    fi
done < requirements.txt


