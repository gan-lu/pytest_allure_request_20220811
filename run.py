#!/usr/bin/python3.7
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# @Time    : 2022/8/18 20:42
# @Author  : mojin
# @Email   : 397135766@qq.com
# @File    : run.py
# @Software: PyCharm
#-------------------------------------------------------------------------------


import pytest,shutil,subprocess
from shutil import copy
from common.read_file import ReadFile
from common.send_email import EmailServe
from common import all_path
from common.exchange_data import ExchangeData

def run():

    setting = dict(ReadFile.read_config('$.email')) #获取邮件相关配置信息
    try:
        shutil.rmtree(all_path.targetPath) #删除allure历史数据
    except:
        pass
    # --reruns = 3  失败重试
    pytest.main(['./test_caes', '-vs', "--env=prod","--reruns=3", "--alluredir", "target/allure-results"])  # pytest测试框架主程序运行


    copy(all_path.Start_server_bat, all_path.targetPath) #拷贝 启动服务器脚本(config/Start_server.bat)，由config目录拷贝到target目录下进行压缩打包发送邮件
    copy(all_path.environmentPath, all_path.allure_results)  # 拷贝 环境配置文件


    allure_html = 'allure generate ./target/allure-results -o ./target/allure-report --clean'  # 生成allure的html报告
    subprocess.call(allure_html, shell=True)  # 生成allure的html报告


    EmailServe.send_email(setting,all_path.targetPath,ExchangeData.get_pytest_summary()) #发送邮件




if __name__ == '__main__':
    run()
